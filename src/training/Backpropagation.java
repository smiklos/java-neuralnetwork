/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training;

import java.util.ArrayList;
import java.util.List;
import neuralnetwork.Classes.NeuralNetwork;
import neuralnetwork.Classes.Neuron;
import neuralnetwork.Classes.OutputNeuron;

/**
 *
 * @author MikiWork
 */
public class Backpropagation extends Training {

    NeuralNetwork nn;
    double realError;
    double errorMean;

    public Backpropagation(NeuralNetwork nn) {
        this.nn = nn;
        this.errorMean = 999;
    }

    public void train() {
        int epoch = 0;

        double sumError = 0;
        double maxError = 999;
        double maxErrorInEpoch = 0;
        List<double[]> inputList = nn.getInputList();
        List<double[]> targetList = nn.getTargetList();
        while (nn.getMaxepoch() > epoch) {
            if (nn.getTargetError() < this.errorMean && nn.getTrgetDifferentError() < maxError) {
                for (int i = 0; i < inputList.size(); i++) {

                    System.out.print("Inputs: ");
                    for (int j = 0; j < inputList.get(i).length; j++) {
                        System.out.print(inputList.get(i)[j] + " ");
                    }
                    System.out.println();
                    System.out.print("Target: ");
                    for (int j = 0; j < targetList.get(i).length; j++) {
                        System.out.print(targetList.get(i)[j] + " ");
                    }
                    System.out.println();

                    nn.setInput(inputList.get(i));
                    nn.setTargetOutput(targetList.get(i));
                    nn.updateWeights();
                    nn.getResult();

                    sumError += this.getTotalError();

                    double temp = this.getMaxError();
                    if (temp > maxErrorInEpoch) {
                        maxErrorInEpoch = temp;
                    }
                    if (i == inputList.size() - 1 && maxError > maxErrorInEpoch) {
                        maxError = maxErrorInEpoch;
                    }

                }
                this.errorMean = sumError / inputList.size();
                sumError = 0;
                System.out.println("**********************************");
                System.out.println("* Epoch Max Error " + epoch + " : " + maxErrorInEpoch);
                System.out.println("**********************************");
                maxErrorInEpoch = 0;

                System.out.println("**********************************");
                System.out.println("* Epoch Error " + epoch + " : " + this.errorMean);
                System.out.println("**********************************");
            } else {
                break;
            }
            epoch++;
        }
        System.out.println("Backpropagation training finished");
    }

    private double getTotalError() {
        double totalError = 0;
        ArrayList<Neuron> outNeuron = this.nn.getOutputLayer().getListOfNeurons();
        for (int i = 0; i < outNeuron.size(); i++) {
            totalError += ((OutputNeuron) outNeuron.get(i)).getError();
        }
        return totalError;
    }

    private double getMaxError() {
        double maxError = 0;
        ArrayList<Neuron> outNeuron = this.nn.getOutputLayer().getListOfNeurons();
        for (int i = 0; i < outNeuron.size(); i++) {
            if (((OutputNeuron) outNeuron.get(i)).getDifferentError() > maxError) {
                maxError = ((OutputNeuron) outNeuron.get(i)).getDifferentError();
            }
        }
        return maxError;
    }

}
