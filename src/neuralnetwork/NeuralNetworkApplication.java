/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork;

import windows.GraphPanel;
import neuralnetwork.Classes.Enums.ActivationFncENUM;
import neuralnetwork.Classes.NeuralNetwork;
import training.Backpropagation;

//swing
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import windows.GraphPanel;

/**
 *
 * @author MikiWork
 */
public class NeuralNetworkApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        NeuralNetworkApplication app = new NeuralNetworkApplication();
        NeuralNetwork nn = app.NeruralNetworkStart();
        List<Double> sublistOfErrorMean = new ArrayList<>(nn.getErrorMeanList().subList((int) (nn.getErrorMeanList().size() * (double) 0.1), nn.getErrorMeanList().size()));
        app.getPlot(sublistOfErrorMean, "ErrorMeanLst");

        List<Double> sublistOfInputsetError = new ArrayList<>(nn.getInputSetMaxErrors().subList((int) (nn.getInputSetMaxErrors().size() * (double) 0.1), nn.getInputSetMaxErrors().size()));
        app.getPlot(sublistOfInputsetError, "Max Errors in each round of Trainset");

    }

    /*private void testSwing(){
        this.setSize(400,400);
        
        Toolkit tk=Toolkit.getDefaultToolkit();
        Dimension dim=tk.getScreenSize();
        
        this.setLocationRelativeTo(null);
        this.setTitle("Neuralnetworks");
        
        JLabel myLabel=new JLabel("Ez egy tesz label");
        JPanel myPanel=new JPanel();
        myPanel.add(myLabel);
        this.add(myPanel);
        
        
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }*/
    private void getPlot(List<Double> dataList, String label) {
        GraphPanel plot = new GraphPanel(dataList);
        plot.setPreferredSize(new Dimension(800, 300));

        JFrame frame = new JFrame(label);
        frame.getContentPane().add(plot);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private NeuralNetwork NeruralNetworkStart() {
        NeuralNetwork nn = new NeuralNetwork(4, 3, new int[]{5, 5, 5}, 1);
        nn.setActivationFnc(ActivationFncENUM.SIGLOG);
        nn.setLearningRate(0.2);
        nn.setMaxEpoch(100);
        nn.setTargetError(0.000000001);
        nn.setTargetDifferentError(0.001);

        nn.setInputList(this.getTrainSetFromCsv("c:/NEST/YHOO.CSV", new int[]{1, 2, 3, 5}, 1));
        nn.setTargetList(this.getTargetListFromCsv("c:/NEST/YHOO.CSV", new int[]{4}, 1));
        /*  nn.addInputData(new double[]{0.0, 0.0});
        nn.addInputData(new double[]{0.0, 1.0});
        nn.addInputData(new double[]{1.0, 0.0});
        nn.addInputData(new double[]{1.0, 1.0});

        nn.addTargetData(new double[]{0.0});
        nn.addTargetData(new double[]{1.0});
        nn.addTargetData(new double[]{1.0});
        nn.addTargetData(new double[]{0.0});
         */

        System.out.println("START NETWORK");
        System.out.println(nn);
        nn.getResult();

        System.out.println("Start Training:");
        Backpropagation bp = new Backpropagation(nn);
        bp.train();
        /* for (int i = 0; i < 50; i++) { 
            nn.setInput(new double[]{0.0,1.0});
            nn.setTargetOutput(new double[]{1.0,0.0});     
            nn.getResult();
            nn.updateWeights();                   
        }*/
        System.out.println("Trained Network");
        System.out.println(nn);
        nn.getResult();
        System.out.println();
        System.out.println("END OF APP");
        return nn;
    }

    public List<double[]> getTrainSetFromCsv(String filePath, int[] columns, int shift) {
        List<double[]> trainSet = new ArrayList<>();
        try {
            FileReader fr = new FileReader(filePath);
            System.out.println("Sikerült megnyitni a fájlt");

            BufferedReader br = new BufferedReader(fr);
            String sor = "";

            int i = 0;
            while (sor != null) {
                sor = br.readLine();
                if (i == 0) {

                    //TODO: do something with headers
                } else if (sor != null) {
                    String[] temp = sor.split(",");
                    double[] row = new double[columns.length];

                    for (int j = 0; j < row.length; j++) {
                        row[j] = Double.parseDouble(temp[columns[j]]);
                    }
                    trainSet.add(row);
                }
                i++;
                System.out.println(sor);
            }

            br.close();

        } catch (FileNotFoundException ex) {

            System.out.println("Nem találom a fájlt." + ex.getMessage());

        } catch (IOException ex) {

            System.out.println(ex.getMessage());

        }
        return trainSet.subList(0, trainSet.size() - 1 - shift);
    }

    public List<double[]> getTargetListFromCsv(String filePath, int[] columns, int shift) {
        List<double[]> targetSet = new ArrayList<>();
        try {
            FileReader fr = new FileReader(filePath);
            System.out.println("Sikerült megnyitni a fájlt");

            BufferedReader br = new BufferedReader(fr);
            String sor = "";

            int i = 0;
            while (sor != null) {
                sor = br.readLine();
                if (i == 0) {
                    //TODO: do something with headers
                    
                } else if (i < shift) {                   
                    //Do nothing
                    
                } else if (sor != null) {
                    
                    String[] temp = sor.split(",");
                    double[] row = new double[columns.length];

                    for (int j = 0; j < row.length; j++) {
                        row[j] = Double.parseDouble(temp[columns[j]]);
                    }
                    targetSet.add(row);
                }
                i++;
                System.out.println(sor);
            }

            br.close();

        } catch (FileNotFoundException ex) {

            System.out.println("Nem találom a fájlt." + ex.getMessage());

        } catch (IOException ex) {

            System.out.println(ex.getMessage());

        }
        return targetSet;
    }

}
