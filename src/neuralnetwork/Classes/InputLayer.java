/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;

/**
 *
 * @author MikiWork
 */
public class InputLayer extends Layer {
        
    protected int numberOfInputs;

    public InputLayer(int numberOfInputs) {
        this.numberOfInputs = numberOfInputs;
    }

    @Override
    public void init() {
        this.neurons = new ArrayList<>();
        for (int i = 0; i < this.numberOfInputs; i++) {
            neurons.add(new InputNeuron(this, this.nextLayer));
        }
    }

    public Layer getNextLayer() {
        return nextLayer;
    }

    public void setNextLayer(Layer nextLayer) {
        this.nextLayer = nextLayer;
    }

    @Override
    public int getNumberOfNeurons() {
        return this.numberOfInputs;
    }

    @Override
    public void prepareNewWeights(double learningRate) {
        throw new UnsupportedOperationException("Inputlayers has no weights to prepare"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateNewWeights() {
        throw new UnsupportedOperationException("Inputlayers has no weights to update"); //To change body of generated methods, choose Tools | Templates.
    }

}
