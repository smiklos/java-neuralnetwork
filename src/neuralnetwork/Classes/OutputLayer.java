/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import neuralnetwork.Classes.Enums.ActivationFncENUM;

/**
 *
 * @author MikiWork
 */
public class OutputLayer extends Layer {

    protected int numberOfOutput;

    public OutputLayer() {
    }

    public OutputLayer(int numberOfOutput) {
        this.numberOfOutput = numberOfOutput;
        this.activationFnc=ActivationFncENUM.LINEAR;
    }

    public int getNumberOfOutput() {
        return numberOfOutput;
    }

    public void setNumberOfOutput(int numberOfOutput) {
        this.numberOfOutput = numberOfOutput;
    }

    @Override
    public void init() {
        for (int i = 0; i < this.numberOfOutput; i++) {
            this.neurons.add(new OutputNeuron(this.previousLayer,this));
        }
    }

    public Layer getPreviousLayer() {
        return previousLayer;
    }

    public void setPreviousLayer(Layer previousLayer) {
        this.previousLayer = previousLayer;
    }

    @Override
    public void prepareNewWeights(double learningRate) {
        for (int i = 0; i < this.numberOfNeurons; i++) {
           this.neurons.get(i).prepareNewWeights(learningRate);
        }
    }
   

    @Override
    public void updateNewWeights() {
        for (int i = 0; i < this.numberOfNeurons; i++) {
            this.neurons.get(i).updateNewWeights();
        }
    }
    
    
    
    

}
