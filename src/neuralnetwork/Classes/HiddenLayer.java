/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import neuralnetwork.Classes.Enums.LayerTypeEnum;

/**
 *
 * @author MikiWork
 */
public class HiddenLayer extends Layer {

    public HiddenLayer(int numberOfNeurons) {
        this.numberOfNeurons = numberOfNeurons;
    }

    @Override
    public void init() {
        if (this.previousLayer != null && this.nextLayer != null) {
            this.neurons = new ArrayList<>();
            for (int i = 0; i < numberOfNeurons; i++) {
                this.neurons.add(new HiddenNeuron(this.previousLayer, this, this.nextLayer));
            }
        } else {
            try {
                throw new Exception("Layers not connected properly!");
            } catch (Exception ex) {
                Logger.getLogger(HiddenLayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Layer getPreviousLayer() {
        return previousLayer;
    }

    public void setPreviousLayer(Layer previousLayer) {
        this.previousLayer = previousLayer;
    }

    public Layer getNextLayer() {
        return nextLayer;
    }

    public void setNextLayer(Layer nextLayer) {
        this.nextLayer = nextLayer;
    }

    @Override
    public void prepareNewWeights(double learningRate) {
       for (int i = 0; i < this.numberOfNeurons; i++) {
           this.neurons.get(i).prepareNewWeights(learningRate);
        }
    }

    @Override
    public void updateNewWeights() {
        for (int i = 0; i < this.numberOfNeurons; i++) {
            this.neurons.get(i).updateNewWeights();
        }
    }
    
    

}
