/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;
import neuralnetwork.Classes.Enums.ActivationFncENUM;

/**
 *
 * @author MikiWork
 */
public abstract class Neuron implements HasValueInteface {

    protected double valueRequestId;
    protected double lastValueRequestId;

    protected static double activationFnc(ActivationFncENUM fnc, double value) {
        switch (fnc) {
            case STEP:
                return fncStep(value);
            case LINEAR:
                return fncLinear(value);
            case SIGLOG:
                return fncSigLog(value);
            case HYPERTAN:
                return fncHyperTan(value);
            default:
                throw new IllegalArgumentException(fnc
                        + " does not exist in ActivationFncENUM");
        }
    }

    public static double derivativeActivationFnc(ActivationFncENUM fnc, double value) {
        switch (fnc) {
            case LINEAR:
                return derivativeFncLinear(value);
            case SIGLOG:
                return derivativeFncSigLog(value);
            case HYPERTAN:
                return derivativeFncHyperTan(value);
            default:
                throw new IllegalArgumentException(fnc
                        + " does not exist in ActivationFncENUM");
        }
    }

    private static double fncStep(double v) {
        if (v >= 0) {
            return 1.0;
        } else {
            return 0.0;
        }
    }

    private static double fncLinear(double v) {
        return v;
    }

    private static double fncSigLog(double v) {
        return (1.0 / (1.0 + Math.exp(-v)));
    }

    private static double fncHyperTan(double v) {
        return Math.tanh(v);
    }

    private static double derivativeFncLinear(double v) {
        return 1.0;
    }

    private static double derivativeFncSigLog(double v) {
        return (v * (1.0 - v));
    }

    private static double derivativeFncHyperTan(double v) {
        return (1.0 / Math.pow(Math.cosh(v), 2.0));
    }

    public abstract void setValue(double value);

    public abstract ArrayList<Weight> getListOfWeight();

    public abstract double getErrorPropagationValue();

    public abstract void prepareNewWeights(double learningRate);
    
    public abstract void resetPropagationValue();

    public abstract void updateNewWeights();
        
    
        
    

}
