/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;

/**
 *
 * @author MikiWork
 */
public class OutputNeuron extends Neuron {

    ArrayList<Weight> weights;
    Layer previousLayer;
    Layer currentLayer;
    private double value;
    private double targetValue;
    private double error;
    private double propagationValue;

    public OutputNeuron(Layer previous, Layer current) {
        weights = new ArrayList<>();
        this.currentLayer = current;
        this.previousLayer = previous;

        for (int i = 0; i < previous.getNumberOfNeurons(); i++) {
            weights.add(new Weight(previousLayer.getListOfNeurons().get(i), this));
        }
        this.error = -111;

    }

    @Override
    public double getValue() {
        if (NeuralNetwork.isNewInput()) {
            double value = 0.0;
            for (int i = 0; i < weights.size(); i++) {
                value += weights.get(i).getWeight() * weights.get(i).getNeuronFrom().getValue();
            }
            value = Neuron.activationFnc(currentLayer.getActivationFnc(), value);
            this.value = value;
        }
        
        return this.value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public ArrayList<Weight> getListOfWeight() {
        return this.weights;
    }

    public void setTargetValue(double value) {
        this.targetValue = value;        
        this.propagationValue=-1 * (this.targetValue - this.value) * Neuron.derivativeActivationFnc(currentLayer.getActivationFnc(), value);
    }

    public double getError() {   
        this.error = 0.5 * (Math.pow(this.targetValue - this.value, 2));
        return this.error;
    }
    public double getDifferentError(){
        return Math.abs(this.value -this.targetValue);
    }

    @Override
    public double getErrorPropagationValue() {       
        return this.propagationValue;
    }

    @Override
    public void prepareNewWeights(double learningRate) {
        for (int i = 0; i < this.weights.size(); i++) {
            this.weights.get(i).prepareNewWeight(learningRate);
        }
    }

    @Override
    public void resetPropagationValue() {
        this.propagationValue=0.0;
    }

    @Override
    public void updateNewWeights() {
       this.propagationValue=0.0;
        for (int i = 0; i < this.weights.size(); i++) {
            this.weights.get(i).updateWeight();
        }
    }
    
   
}
