/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;
import java.util.List;
import neuralnetwork.Classes.Enums.ActivationFncENUM;

/**
 *
 * @author MikiWork
 */
public class NeuralNetwork {

    ArrayList<Layer> layers;
    int numberOfOutput;
    private double learningRate;
    private List<double[]> inputList;

    
    private List<double[]> targetList;
    private int maxEpoch;
    private double targetError;
    private double realError;

    

    private static boolean newInput = true;

    public static boolean isNewInput() {
        return NeuralNetwork.newInput;
    }
    private double targetDifferentError;
    

    public NeuralNetwork(int numberOfInputs, int numberOfHiddenLayers, int[] neuronsInLayers, int numberOfOutputs) {
        layers = new ArrayList<>();
        InputLayer input = new InputLayer(numberOfInputs);
        layers.add(input);
        for (int i = 0; i < numberOfHiddenLayers; i++) {
            layers.add(new HiddenLayer(neuronsInLayers[i]));
        }
        OutputLayer output = new OutputLayer(numberOfOutputs);
        layers.add(output);
        this.numberOfOutput = numberOfOutputs;

        input.setNextLayer(layers.get(1));
        ((HiddenLayer) layers.get(1)).setPreviousLayer(input);
        for (int i = 1; i < layers.size() - 2; i++) {
            ((HiddenLayer) layers.get(i)).setNextLayer(layers.get(i + 1));
            ((HiddenLayer) layers.get(i + 1)).setPreviousLayer(layers.get(i));
        }
        ((HiddenLayer) layers.get(layers.size() - 2)).setNextLayer(output);
        output.setPreviousLayer(layers.get(layers.size() - 2));

        for (int i = 0; i < layers.size(); i++) {
            layers.get(i).init();
        }

        this.inputList = new ArrayList<>();
        this.targetList = new ArrayList<>();
    }

    public void addInputRow(double[] inputs) {
        this.inputList.add(inputs);
    }

    public void addTargetRow(double[] targets) {
        this.targetList.add(targets);
    }

    public void setActivationFnc(ActivationFncENUM activationFnc) {
        for (int i = 1; i < layers.size() - 1; i++) {
            layers.get(i).setActivationFnc(activationFnc);
        }
    }

    public void setActivationFncOutput(ActivationFncENUM activationFnc) {
        this.getOutputLayer().setActivationFnc(activationFnc);
    }

    public void setInput(double[] inputs) {

        int numberOfInput = layers.get(0).getNumberOfNeurons();
        if (numberOfInput != inputs.length) {
            System.out.println("Wrong number of input data! expected: " + numberOfInput);
            return;
        }

        for (int i = 0; i < inputs.length; i++) {
            layers.get(0).getListOfNeurons().get(i).setValue(inputs[i]);
        }

    }

    /**
     * Also calculates propagation values to fix error
     *
     * @param targets
     */
    public void setTargetOutput(double[] targets) {

        int numberOfOutput = layers.get(layers.size() - 1).getNumberOfNeurons();
        if (numberOfOutput != targets.length) {
            System.out.println("Wrong number of target data! expected: " + numberOfOutput);
            return;
        }
        int i = 0;
        for (Neuron outputNeuron : this.getOutputLayer().getListOfNeurons()) {
            ((OutputNeuron) outputNeuron).setTargetValue(targets[i]);
            i++;
        }

    }

    public void getResult() {
        NeuralNetwork.newInput = true;
        double[] results = new double[this.numberOfOutput];
        double[] errors = new double[this.numberOfOutput];
        int i = 0;
        for (Neuron outputNeuron : this.getOutputLayer().getListOfNeurons()) {
            double outputX = outputNeuron.getValue();
            double errorX = ((OutputNeuron) outputNeuron).getError();
            results[i] = outputX;
            errors[i] = errorX;
            i++;
        }
        System.out.println("Outputs: ");
        for (double output : results) {
            System.out.print(output + ", ");
        }
        System.out.println();
        System.out.println("Errors: ");
        for (double error : errors) {
            System.out.print(error + ", ");
        }
        System.out.println();
        System.out.println("-------------------------------------------------");
        NeuralNetwork.newInput = false;
    }

    public void updateWeights() {
        for (int i = this.layers.size() - 1; i > 0; i--) {
            layers.get(i).prepareNewWeights(this.learningRate);
        }
        for (int i = this.layers.size() - 1; i > 0; i--) {
            layers.get(i).updateNewWeights();
        }

    }

    public Layer getOutputLayer() {
        return layers.get(layers.size() - 1);
    }

    @Override
    public String toString() {
        String st = "";
        //input
        st += "## Input Layer: ## \n";
        st += "Inputs: \n";
        for (int i = 0; i < this.layers.get(0).getListOfNeurons().size(); i++) {
            st += this.layers.get(0).getListOfNeurons().get(i).getValue() + ", ";
        }
        st += "\n";
        //hidden + output
        for (int i = 1; i < layers.size(); i++) {
            st += "## Layer " + i + ": ## \n";
            for (int j = 0; j < layers.get(i).getListOfNeurons().size(); j++) {
                st += "* Neuron " + j + ": * \n";
                for (int k = 0; k < layers.get(i).getListOfNeurons().get(j).getListOfWeight().size(); k++) {
                    st += layers.get(i).getListOfNeurons().get(j).getListOfWeight().get(k).getWeight() + ", ";
                }
                st += "\n";
            }
        }
        return st;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public void addInputData(double[] d) {
        if (this.inputList == null) {
            this.inputList = new ArrayList<>();
        }
        this.inputList.add(d);
    }

    public void addTargetData(double[] d) {
        if (this.targetList == null) {
            this.targetList = new ArrayList<>();
        }
        this.targetList.add(d);
    }

    public void setMaxEpoch(int i) {
        this.maxEpoch = i;
    }

    public void setTargetError(double d) {
        this.targetError = d;
    }

    public int getMaxepoch() {
        return this.maxEpoch;
    }

    public double getTargetError() {
       return this.targetError;
    }
    
    public List<double[]> getInputList() {
        return inputList;
    }

    public List<double[]> getTargetList() {
        return targetList;
    }
    
    public double getRealError() {
        return realError;
    }

    public void setRealError(double realError) {
        this.realError = realError;
    }

    public void setTargetDifferentError(double d) {
        this.targetDifferentError=d;
    }
    
    public double getTrgetDifferentError(){
        return this.targetDifferentError;
    }
    
    

}
