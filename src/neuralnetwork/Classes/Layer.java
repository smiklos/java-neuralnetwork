/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;
import neuralnetwork.Classes.Enums.ActivationFncENUM;

/**
 *
 * @author MikiWork
 */
public abstract class Layer {
    
    protected ArrayList<Neuron> neurons;
    protected Layer previousLayer;
    protected Layer nextLayer;
    protected int numberOfNeurons;
    protected ActivationFncENUM activationFnc;

    public abstract void init();

    public Layer() {
        neurons=new ArrayList<>();        
    }
    
    
    
    public ActivationFncENUM getActivationFnc() {
        return activationFnc;
    }  

    public void setActivationFnc(ActivationFncENUM activationFnc) {
        this.activationFnc = activationFnc;
    }
    
    public ArrayList<Neuron> getListOfNeurons() {
        return this.neurons;
    }
    
    public int getNumberOfNeurons() {
        if (this.numberOfNeurons == 0) {
            this.numberOfNeurons = this.neurons.size();
        }
        return this.numberOfNeurons;
    }
    
    public abstract void prepareNewWeights(double learningRate);
    
    public abstract void updateNewWeights();
    
    
            
}
