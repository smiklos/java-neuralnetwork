/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.Random;

/**
 *
 * @author MikiWork
 */
public class Weight {
    private Neuron from;
    private Neuron to;
    private double weight;
    private double newWeight;
    
    static Random r;

    public Weight(Neuron from,Neuron to) {
        if(r == null){
            r=new Random();
        }
        this.weight=Weight.r.nextDouble();
        this.from=from;
        this.to=to;
    }
    
    

    public Neuron getNeuronFrom() {
        return from;
    }
    
    public Neuron getNeuronTo() {
        return to;
    }
    
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public void prepareNewWeight(double learningRate){
        this.newWeight=this.weight-(learningRate*to.getErrorPropagationValue()*from.getValue());
    }
    
    public void updateWeight(){
        this.weight=this.newWeight;
    }
    
}
