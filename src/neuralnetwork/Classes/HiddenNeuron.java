/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;
import neuralnetwork.Classes.Enums.LayerTypeEnum;

/**
 *
 * @author MikiWork
 */
public class HiddenNeuron extends Neuron {

    private ArrayList<Weight> weights;
    private Layer current;
    private Layer previous;
    private Layer next;
    private double value;
    private double errorPropagationValue;

    public HiddenNeuron(Layer previous, Layer current, Layer next) {
        weights = new ArrayList<>();
        this.current = current;
        this.previous = previous;
        this.next = next;

        for (int i = 0; i < previous.getNumberOfNeurons(); i++) {
            //connect Neurons with weight
            this.weights.add(new Weight(previous.getListOfNeurons().get(i), this));
        }

    }

    @Override
    public double getValue() {
        if (NeuralNetwork.isNewInput()) {
            double value = 0.0;
            for (int i = 0; i < weights.size(); i++) {
                value += weights.get(i).getWeight() * weights.get(i).getNeuronFrom().getValue();
            }
            this.value = Neuron.activationFnc(current.getActivationFnc(), value);
        }
        return this.value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public ArrayList<Weight> getListOfWeight() {
        return this.weights;
    }

    @Override
    public double getErrorPropagationValue() {
        if (this.errorPropagationValue == 0.0) {
            double errorProp = 0.0;
            for (int i = 0; i < this.next.getListOfNeurons().size(); i++) {
                Weight weight = null;
                int j = 0;
                while (weight == null && j < this.next.getListOfNeurons().get(i).getListOfWeight().size()) {
                    if (this.next.getListOfNeurons().get(i).getListOfWeight().get(j).getNeuronFrom().equals(this)) {
                        weight = this.next.getListOfNeurons().get(i).getListOfWeight().get(j);                        
                    }
                    j++;
                }
                if (weight != null) {
                    errorProp += this.next.getListOfNeurons().get(i).getErrorPropagationValue() * weight.getWeight();
                }
            }
            errorProp *= this.derivativeActivationFnc(current.getActivationFnc(), this.value);
            this.errorPropagationValue = errorProp;
        }
        return this.errorPropagationValue;
    }

    @Override
    public void prepareNewWeights(double learningRate) {
        for (int i = 0; i < this.weights.size(); i++) {
            this.weights.get(i).prepareNewWeight(learningRate);
        }
    }

    @Override
    public void resetPropagationValue() {
        this.errorPropagationValue = 0.0;
    }

    @Override
    public void updateNewWeights() {
        this.errorPropagationValue=0.0;
        for (int i = 0; i < this.weights.size(); i++) {
            this.weights.get(i).updateWeight();
        }
    }

}
