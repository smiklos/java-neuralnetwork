/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork.Classes;

import java.util.ArrayList;

/**
 *
 * @author MikiWork
 */
public class InputNeuron extends Neuron{
    
    private Layer nextLayer;
    private Layer currentLayer;
    private static ArrayList<Weight> weight;
    
    private double value;
    
    public InputNeuron(Layer current,Layer nextLayer){
        this.nextLayer=nextLayer;
        this.currentLayer=currentLayer;
        
        if(this.weight == null){
            InputNeuron.weight=new ArrayList<>();
        }
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }
     
        
    @Override
    public double getValue() {
        return this.value;
    }        

    @Override
    public ArrayList<Weight> getListOfWeight() {
        return weight;
    }

    @Override
    public double getErrorPropagationValue() {
        throw new UnsupportedOperationException("Inputneuron has no propagation value."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void prepareNewWeights(double learningRate) {
        throw new UnsupportedOperationException("Inputneuron has no weights."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resetPropagationValue() {
        throw new UnsupportedOperationException("Inputneuron has no propagationValue"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateNewWeights() {
        throw new UnsupportedOperationException("Inputneurons has no weights"); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
